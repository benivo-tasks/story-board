﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoryBoard.Business.Models
{
    public class Story
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(500)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        [MaxLength(5000)]
        public string Content { get; set; }
        public DateTime PostedOn { get; set; } = DateTime.Now;
        public int[] GroupIds { get; set; }
        //public int UserId { get; set; }
    }
}
