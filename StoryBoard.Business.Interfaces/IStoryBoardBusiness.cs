﻿using StoryBoard.Business.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoryBoard.Business.Interfaces
{
    public interface IStoryBoardBusiness
    {
        //User
        Task<User> GetUser(int id);

        Task<IEnumerable<User>> GetUsers();

        Task<User> CreateUser(User user);

        Task<User> UpdateUser(int id, User user);

        Task DeleteUser(int id);

        //Story
        Task<Story> GetStory(int id);

        Task<IEnumerable<Story>> GetStories();

        Task<Story> CreateStory(Story story);

        Task<Story> UpdateStory(int id, Story story);

        Task DeleteStory(int id);

        //Group
        Task<Group> GetGroup(int id);

        Task<IEnumerable<Group>> GetGroups();

        Task<Group> CreateGroup(Group group);

        Task<Group> UpdateGroup(int id, Group group);

        Task DeleteGroup(int id);


        Task<IEnumerable<Story>> GetUserStories(int userId);
        Task<Story> CreateUserStory(int userId, Story story);
        Task JoinGroup(int userId, int groupId);
        Task LeaveGroup(int userId, int groupId);
        Task<IEnumerable<Group>> GetUserGroups(int userId);
        Task<Group> CreateUserGroups(int userId, Group group);
        Task<IEnumerable<Story>> GetGroupStories(int groupId);
        Task<User> GetUserByName(string name);
    }
}
