﻿using StoryBoard.Business.Interfaces;
using System;
using StoryBoard.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using StoryBoard.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Mapster;
using System.Collections;

namespace StoryBoard.Business
{
    public class StoryBoardBusiness : StoryBoardBusinessBase, IStoryBoardBusiness
    {
        public StoryBoardBusiness(StoryBoardContext storyBoardContext, IAdapter mapper) : base(storyBoardContext, mapper) { }
        #region User

        public Task<User> GetUser(int id)
        {
            return Get<Data.Models.User, User>(id);
        }

        public Task<IEnumerable<User>> GetUsers()
        {
            return Get<Data.Models.User, User>();
        }
        public Task<User> CreateUser(User user)
        {
            return Create<Data.Models.User, User>(user);
        }
        public Task<User> UpdateUser(int id, User user)
        {
            return Update<Data.Models.User, User>(id, user);
        }
        public Task DeleteUser(int id)
        {
            return Delete<Data.Models.User, User>(id);
        }

        #endregion

        #region Story

        public async Task<Story> GetStory(int id)
        {
            var story = await _storyBoardContext.Stories
                .Select(s => new Story
                {
                    Id = s.Id,
                    Title = s.Title,
                    Description = s.Description,
                    Content = s.Content,
                    PostedOn = s.PostedOn,
                    GroupIds = s.StoryGroups.Select(sg => sg.GroupId).ToArray()
                })
                .FirstOrDefaultAsync(s => s.Id == id);
            return story;

        }
        public Task<IEnumerable<Story>> GetStories()
        {
            return Get<Data.Models.Story, Story>();
        }

        public Task<Story> CreateStory(Story story)
        {
            return Create<Data.Models.Story, Story>(story);
        }

        public Task<Story> UpdateStory(int id, Story story)
        {
            return Update<Data.Models.Story, Story>(id, story);
        }
        public Task DeleteStory(int id)
        {
            return Delete<Data.Models.Story, Story>(id);
        }

        #endregion

        #region Group

        public Task<Group> GetGroup(int id)
        {
            return Get<Data.Models.Group, Group>(id);
        }

        public async Task<IEnumerable<Group>> GetGroups()
        {
            var result = from gr in _storyBoardContext.Groups.Include(g => g.StoryGroups).Include(g => g.UserGroups)
                         select new Group
                         {
                             Id = gr.Id,
                             Name = gr.Name,
                             Description = gr.Description,
                             StoryCount = gr.StoryGroups.Count,
                             UserCount = gr.UserGroups.Count
                         };
            return await result.ToListAsync();
        }
        public Task<Group> CreateGroup(Group group)
        {
            return Create<Data.Models.Group, Group>(group);
        }
        public Task<Group> UpdateGroup(int id, Group group)
        {
            return Update<Data.Models.Group, Group>(id, group);
        }
        public Task DeleteGroup(int id)
        {
            return Delete<Data.Models.Group, Group>(id);
        }

        #endregion

        public async Task<IEnumerable<Story>> GetUserStories(int userId)
        {
            var stories = from s in _storyBoardContext.Stories
                          where s.UserId == userId
                          select new Story
                          {
                              Id = s.Id,
                              Title = s.Title,
                              Description = s.Description,
                              PostedOn = s.PostedOn
                          };

            return await stories.ToListAsync();
        }

        public async Task<Story> CreateUserStory(int userId, Story story)
        {
            var dbSet = _storyBoardContext.Stories;
            var entity = _mapper.Adapt<Story, Data.Models.Story>(story);
            if (story.GroupIds != null && story.GroupIds.Any())
            {
                entity.StoryGroups = story.GroupIds.Select(gId => new Data.Models.StoryGroup { GroupId = gId }).ToList();
            }
            entity.UserId = userId;
            dbSet.Add(entity);
            await _storyBoardContext.SaveChangesAsync();
            return _mapper.Adapt<Data.Models.Story, Story>(entity);
        }

        public async Task JoinGroup(int userId, int groupId)
        {
            var user = await _storyBoardContext.Users.Include(u => u.UserGroups)
                .SingleOrDefaultAsync(u => u.Id == userId && u.UserGroups.All(ug => ug.GroupId != groupId));

            if (user != null)
            {
                user.UserGroups.Add(new Data.Models.UserGroup { GroupId = groupId });
                await _storyBoardContext.SaveChangesAsync();
            }
        }
        public async Task LeaveGroup(int userId, int groupId)
        {
            var user = await _storyBoardContext.Users.Include(u => u.UserGroups).SingleOrDefaultAsync(u => u.Id == userId && u.UserGroups.Any(ug => ug.GroupId == groupId));

            if (user != null)
            {
                var userGroup = user.UserGroups.SingleOrDefault(ug => ug.GroupId == groupId);
                user.UserGroups.Remove(userGroup);
                await _storyBoardContext.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Group>> GetUserGroups(int userId)
        {
            var user = await _storyBoardContext.Users.Include(u => u.UserGroups).ThenInclude(ug => ug.Group).SingleOrDefaultAsync(u => u.Id == userId);
            if (user != null)
            {
                return _mapper.Adapt<IEnumerable<Data.Models.Group>, IEnumerable<Group>>(user.UserGroups.Select(ug => ug.Group));
            }
            return null;
        }

        public async Task<Group> CreateUserGroups(int userId, Group group)
        {
            var dbGroup = _mapper.Adapt<Group, Data.Models.Group>(group);
            dbGroup.UserGroups.Add(new Data.Models.UserGroup { UserId = userId });
            _storyBoardContext.Groups.Add(dbGroup);
            await _storyBoardContext.SaveChangesAsync();
            return _mapper.Adapt<Data.Models.Group, Group>(dbGroup);
        }

        public async Task<IEnumerable<Story>> GetGroupStories(int groupId)
        {
            var stories = from s in _storyBoardContext.Stories.Include(s => s.StoryGroups)
                          from sg in s.StoryGroups
                          where sg.GroupId == groupId
                          select sg.Story;

            return _mapper.Adapt<IEnumerable<Data.Models.Story>, IEnumerable<Story>>(await stories.ToListAsync());
        }

        public async Task<User> GetUserByName(string name)
        {
            var user = await _storyBoardContext.Users.FirstOrDefaultAsync(u => u.Name == name);
            return _mapper.Adapt<Data.Models.User, User>(user);
        }
    }
}
