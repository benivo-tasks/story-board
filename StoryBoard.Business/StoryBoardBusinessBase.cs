﻿using Microsoft.EntityFrameworkCore;
using StoryBoard.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StoryBoard.Business
{
    public abstract class StoryBoardBusinessBase : IDisposable
    {
        protected readonly StoryBoardContext _storyBoardContext;
        protected readonly Mapster.IAdapter _mapper;

        protected StoryBoardBusinessBase(StoryBoardContext storyBoardContext, Mapster.IAdapter mapper)
        {
            _mapper = mapper;
            _storyBoardContext = storyBoardContext;
        }

        protected async Task<TDestination> Get<TSource, TDestination>(int id) where TSource : class where TDestination : class
        {
            var dbSet = _storyBoardContext.Set<TSource>();
            var entity = await dbSet.FindAsync(id);
            return entity != null ? _mapper.Adapt<TSource, TDestination>(entity) : null;
        }
        protected async Task<IEnumerable<TDestination>> Get<TSource, TDestination>() where TSource : class where TDestination : class
        {
            var dbSet = _storyBoardContext.Set<TSource>();
            var entities = await dbSet.ToListAsync();
            return _mapper.Adapt<List<TSource>, IEnumerable<TDestination>>(entities);
        }

        protected async Task<TDestination> Create<TSource, TDestination>(TDestination obj) where TSource : class where TDestination : class
        {
            var dbSet = _storyBoardContext.Set<TSource>();
            var entity = _mapper.Adapt<TDestination, TSource>(obj);
            dbSet.Add(entity);
            await _storyBoardContext.SaveChangesAsync();
            return _mapper.Adapt<TSource, TDestination>(entity);
        }

        protected async Task<TDestination> Update<TSource, TDestination>(int id, TDestination obj) where TSource : class where TDestination : class
        {
            var dbSet = _storyBoardContext.Set<TSource>();
            var entity = await dbSet.FindAsync(id);
            _mapper.Adapt(obj, entity);
            dbSet.Update(entity);
            await _storyBoardContext.SaveChangesAsync();
            return _mapper.Adapt<TSource, TDestination>(entity);
        }

        protected async Task Delete<TSource, TDestination>(int id) where TSource : class where TDestination : class
        {
            var dbSet = _storyBoardContext.Set<TSource>();
            var entity = await dbSet.FindAsync(id);
            if (entity != null)
            {
                dbSet.Remove(entity);
                await _storyBoardContext.SaveChangesAsync();
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _storyBoardContext?.Dispose();
            }
        }
    }
}
