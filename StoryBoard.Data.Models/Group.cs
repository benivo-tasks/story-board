﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoryBoard.Data.Models
{
    public class Group
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public ICollection<StoryGroup> StoryGroups { get; set; } = new HashSet<StoryGroup>();
        public ICollection<UserGroup> UserGroups { get; set; } = new HashSet<UserGroup>();
    }
}
