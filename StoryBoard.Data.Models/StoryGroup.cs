﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoryBoard.Data.Models
{
    public class StoryGroup
    {
        public int StoryId { get; set; }
        public Story Story { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
