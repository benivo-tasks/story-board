﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoryBoard.Data.Models
{
    public class Story
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [Required]
        [MaxLength(5000)]
        public string Content { get; set; }
        public DateTime PostedOn { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<StoryGroup> StoryGroups { get; set; } = new HashSet<StoryGroup>();

    }
}
