using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoryBoard.Business.Interfaces;
using StoryBoard.Business.Models;

namespace StoryBoard.Web.Controllers
{
    [Route("api/Stories")]
    public class StoriesController : BaseController
    {
        public StoriesController(IStoryBoardBusiness storyBoardBusiness) : base(storyBoardBusiness) { }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var result = await _storyBoardBusiness.GetStory(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _storyBoardBusiness.GetStories();
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Story story)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //hardcoded userId
                var result = await _storyBoardBusiness.CreateStory(story);
                return CreatedAtAction("Get", new { Id = result.Id }, result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Story story)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            story.Id = id;
            var result = await _storyBoardBusiness.UpdateStory(id, story);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _storyBoardBusiness.DeleteStory(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("/api/users/{userId}/stories")]
        public async Task<IActionResult> GetUserStories(int userId)
        {
            try
            {
                var result = await _storyBoardBusiness.GetUserStories(userId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("/api/users/{userId}/stories")]
        public async Task<IActionResult> CreateUserStory(int userId, [FromBody] Story story)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var result = await _storyBoardBusiness.CreateUserStory(userId, story);
                return CreatedAtAction("GetUserStories", new { userId = userId }, result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("/api/groups/{groupId}/stories")]
        public async Task<IActionResult> GetGroupStories(int groupId)
        {
            try
            {
                var result = await _storyBoardBusiness.GetGroupStories(groupId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

    }
}