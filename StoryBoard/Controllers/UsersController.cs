using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoryBoard.Business.Interfaces;
using StoryBoard.Business.Models;

namespace StoryBoard.Web.Controllers
{
    [Route("api/Users")]
    public class UsersController : BaseController
    {
        public UsersController(IStoryBoardBusiness storyBoardBusiness) : base(storyBoardBusiness) { }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var result = await _storyBoardBusiness.GetUser(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _storyBoardBusiness.GetUsers();
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var result = await _storyBoardBusiness.CreateUser(user);
                return CreatedAtAction("Get", new { Id = result.Id }, result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] User user)
        {
            try
            {
                user.Id = id;
                var result = await _storyBoardBusiness.UpdateUser(id, user);
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _storyBoardBusiness.DeleteUser(id);
                return NoContent();
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("search")]
        public async Task<IActionResult> GetByName([FromQuery] string name)
        {
            try
            {
                var result = await _storyBoardBusiness.GetUserByName(name);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("/api/users/{userId}/groups/{groupId}/join")]
        public async Task<IActionResult> JoinGroup(int userId, int groupId)
        {
            try
            {
                await _storyBoardBusiness.JoinGroup(userId, groupId);
                return NoContent();
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("/api/users/{userId}/groups/{groupId}/leave")]
        public async Task<IActionResult> LeaveGroup(int userId, int groupId)
        {
            try
            {
                await _storyBoardBusiness.LeaveGroup(userId, groupId);
                return NoContent();
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }
    }
}