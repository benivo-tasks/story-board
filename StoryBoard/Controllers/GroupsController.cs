using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoryBoard.Business.Interfaces;
using StoryBoard.Business.Models;

namespace StoryBoard.Web.Controllers
{
    [Route("api/Groups")]
    public class GroupsController : BaseController
    {
        public GroupsController(IStoryBoardBusiness storyBoardBusiness) : base(storyBoardBusiness) { }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var result = await _storyBoardBusiness.GetGroup(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _storyBoardBusiness.GetGroups();
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Group group)
        {
            try
            {
                var result = await _storyBoardBusiness.CreateGroup(group);
                return CreatedAtAction("Get", new { Id = result.Id }, result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Group group)
        {
            try
            {
                group.Id = id;
                var result = await _storyBoardBusiness.UpdateGroup(id, group);
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _storyBoardBusiness.DeleteGroup(id);
                return NoContent();
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("/api/users/{userId}/groups")]
        public async Task<IActionResult> GetUserGroups(int userId)
        {
            try
            {
                var result = await _storyBoardBusiness.GetUserGroups(userId);
                return Ok(result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("/api/users/{userId}/groups")]
        public async Task<IActionResult> CreateUserGroup(int userId, [FromBody] Group group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var result = await _storyBoardBusiness.CreateUserGroups(userId, group);
                return CreatedAtAction("Get", new { result.Id }, result);
            }
            catch (Exception e)
            {
                //expose exception message during development. In production must be handled with custom messages.
                return StatusCode(500, e.Message);
            }
        }
    }
}