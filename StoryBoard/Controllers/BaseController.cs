using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoryBoard.Business.Interfaces;

namespace StoryBoard.Web.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class BaseController : Controller
    {
        protected readonly IStoryBoardBusiness _storyBoardBusiness;
        public BaseController(IStoryBoardBusiness storyBoardBusiness) : base()
        {
            _storyBoardBusiness = storyBoardBusiness;
        }
    }
}