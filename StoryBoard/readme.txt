﻿Project is created by using ASP.NET Core, Entity Framework Core for back-end and React + Redux for front-end.
Database is created with Entity Framework Code First migrations.
Database connection string is set in the appsettings.json file. You can change it to your database. Initially LocalDb is used.
SPA bundles are built and located in wwwroot.
The app needs at least one user in the database. Create users in an empty database directly or by Web API after starting application, making POST request to endpoint "/api/users" with request body json using template: { "name": "<NAME>"}.
