﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using StoryBoard.Data;

namespace StoryBoard.Web.Migrations
{
    [DbContext(typeof(StoryBoardContext))]
    [Migration("20170713212306_Added_StoryGroups")]
    partial class Added_StoryGroups
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("StoryBoard.Data.Models.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.HasKey("Id");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("StoryBoard.Data.Models.Story", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<string>("Description");

                    b.Property<DateTime>("PostedOn");

                    b.Property<string>("Title");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Stories");
                });

            modelBuilder.Entity("StoryBoard.Data.Models.StoryGroup", b =>
                {
                    b.Property<int>("StoryId");

                    b.Property<int>("GroupId");

                    b.HasKey("StoryId", "GroupId");

                    b.HasIndex("GroupId");

                    b.ToTable("StoryGroup");
                });

            modelBuilder.Entity("StoryBoard.Data.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("Name");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("StoryBoard.Data.Models.Story", b =>
                {
                    b.HasOne("StoryBoard.Data.Models.User", "User")
                        .WithMany("Stories")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("StoryBoard.Data.Models.StoryGroup", b =>
                {
                    b.HasOne("StoryBoard.Data.Models.Group", "Group")
                        .WithMany("StoryGroups")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("StoryBoard.Data.Models.Story", "Story")
                        .WithMany("StoryGroups")
                        .HasForeignKey("StoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
