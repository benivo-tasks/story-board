import { compile as compileUrl } from "path-to-regexp";
import { stringify } from "qs";
import forEach from "lodash/forEach";

export default class ApiClient {
    constructor(config) {
        this.config = config;
        this.rootUrl = this.config.rootUrl;
        this.apiBasePath = this.config.apiBasePath;
        this.createApiMethods();
    }
    get baseUrl() {
        return `${this.rootUrl ? this.rootUrl : ""}${this.apiBasePath}`;
    }

    createFetch(routePath, { method = "GET" } = {}) {
        let mainUrlCreator = compileUrl(routePath);
        const urlCreators = {};
        return async (params = {}) => {
            let urlCreator = mainUrlCreator;
            const { path, query, ...others } = params;
            if (path) {
                if (urlCreators[path] === undefined) {
                    urlCreators[path] = compileUrl(path);
                }
                urlCreator = urlCreators[path];
            }
            let url = `${this.baseUrl}${urlCreator(others)}`;

            const queryString = stringify(query);
            if (queryString) {
                url += `?${queryString}`;
            }
            const options = {};
            const headers = new Headers();
            options.method = method;
            if (params.data) {
                options.body = JSON.stringify(params.data);
                headers.append("Content-Type", "application/json");
            }
            options.headers = headers;
            const response = await fetch(url, options);
            if (response.ok) {
                return await response.json();
            } else {
                throw new Error(response.statusText);
            }
        }
    }

    createApiMethods() {
        const resources = this.config.resources;
        forEach(resources, (resource, key) => {
            resource.forEach(item => this[item.action] = this.createFetch(item.url, { method: item.method }));
        });
    }
}
