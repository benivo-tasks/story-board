import { createAction } from "redux-act";

export const createActions = (name) => {
    const successActionName = `${name}-SUCCESS`;
    const failureActionName = `${name}-FAILURE`;
    return {
        start: createAction(name),
        success: createAction(successActionName),
        failure: createAction(failureActionName)
    };
};
