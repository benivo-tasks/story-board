import { createActions } from "../utils/createActions";
import { createReducer, createAction } from "redux-act";

const getStoryActions = createActions("GET_STORY");
const createStoryActions = createActions("CREATE_STORY");
const updateStoryActions = createActions("UPDATE_STORY");
const deleteStoryActions = createActions("DELETE_STORY");
export const clearStory = createAction("CLEAR_STORY");

export const getStory = ({ id }) => async (dispatch, getState, { apiClient }) => {
    dispatch(getStoryActions.start());
    try {
        const result = await apiClient.getStory({ id });
        return dispatch(getStoryActions.success(result));
    } catch (error) {
        dispatch(getStoryActions.failure(error.message));
    }
};
export const createStory = ({ userId, data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(createStoryActions.start());
    try {
        const result = await apiClient.createStory({ userId, data });
        return dispatch(createStoryActions.success(result));
    } catch (error) {
        dispatch(createStoryActions.failure(error.message));
    }
};
export const updateStory = ({ id, data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(updateStoryActions.start());
    try {
        const result = await apiClient.updateStory({ id, data });
        return dispatch(updateStoryActions.success(result));
    } catch (error) {
        dispatch(updateStoryActions.failure(error.message));
    }
};
export const deleteStory = ({ id }) => async (dispatch, getState, { apiClient }) => {
    dispatch(deleteStoryActions.start());
    try {
        const result = await apiClient.deleteStory({ id });
        return dispatch(deleteStoryActions.success(result));
    } catch (error) {
        dispatch(deleteStoryActions.failure(error.message));
    }
};

export const reducer = createReducer({
    [getStoryActions.start]: (state, payload) => ({ ...state, loading: true }),
    [getStoryActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [getStoryActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [createStoryActions.start]: (state, payload) => ({ ...state, loading: true }),
    [createStoryActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [createStoryActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [updateStoryActions.start]: (state, payload) => ({ ...state, loading: true }),
    [updateStoryActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [updateStoryActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [deleteStoryActions.start]: (state, payload) => ({ ...state, loading: true }),
    [deleteStoryActions.success]: (state, payload) => ({ loading: false }),
    [deleteStoryActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [clearStory]: (state, payload) => ({ loading: false })
}, {});
