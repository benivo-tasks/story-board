import { createActions } from "../utils/createActions";
import { createReducer } from "redux-act";

const getUserActions = createActions("GET_USER");
const createUserActions = createActions("CREATE_USER");
const updateUserActions = createActions("UPDATE_USER");
const deleteUserActions = createActions("DELETE_USER");

export const getUser = ({ id, name }) => async (dispatch, getState, { apiClient }) => {
    dispatch(getUserActions.start());
    try {
        let result;
        if (name) {
            result = await apiClient.getUserByName({ query: { name } });
        } else if (id) {
            result = await apiClient.getUser({ id });
        }
        return dispatch(getUserActions.success(result));
    } catch (error) {
        dispatch(getUserActions.failure(error.message));
    }
};
export const createUser = ({ data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(createUserActions.start());
    try {
        const result = await apiClient.createUser({ data });
        return dispatch(createUserActions.success(result));
    } catch (error) {
        dispatch(createUserActions.failure(error.message));
    }
};
export const updateUser = ({ id, data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(updateUserActions.start());
    try {
        const result = await apiClient.updateUser({ id, data });
        return dispatch(updateUserActions.success(result));
    } catch (error) {
        dispatch(updateUserActions.failure(error.message));
    }
};
export const deleteUser = ({ id }) => async (dispatch, getState, { apiClient }) => {
    dispatch(deleteUserActions.start());
    try {
        const result = await apiClient.deleteUser({ id });
        return dispatch(deleteUserActions.success(result));
    } catch (error) {
        dispatch(deleteUserActions.failure(error.message));
    }
};

export const reducer = createReducer({
    [getUserActions.start]: (state, payload) => ({ ...state, loading: true }),
    [getUserActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [getUserActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [createUserActions.start]: (state, payload) => ({ ...state, loading: true }),
    [createUserActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [createUserActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [updateUserActions.start]: (state, payload) => ({ ...state, loading: true }),
    [updateUserActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [updateUserActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [deleteUserActions.start]: (state, payload) => ({ ...state, loading: true }),
    [deleteUserActions.success]: (state, payload) => ({ loading: false }),
    [deleteUserActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload })
}, {});
