import { createActions } from "../utils/createActions";
import { createReducer } from "redux-act";

const storiesActions = createActions("GET_STORIES");

export const getStories = ({ userId }) => async (dispatch, getState, { apiClient }) => {
    dispatch(storiesActions.start());
    try {
        const stories = await apiClient.getStories({ userId });
        return dispatch(storiesActions.success(stories));
    } catch (error) {
        dispatch(storiesActions.failure(error.message));
    }
};

export const reducer = createReducer({
    [storiesActions.start]: (state, payload) => ({ ...state, loading: true }),
    [storiesActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [storiesActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload })
}, {});
