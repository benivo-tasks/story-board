import { createActions } from "../utils/createActions";
import { createReducer } from "redux-act";

const getGroupActions = createActions("GET_GROUP");
const createGroupActions = createActions("CREATE_GROUP");
const updateGroupActions = createActions("UPDATE_GROUP");
const deleteGroupActions = createActions("DELETE_GROUP");

export const getGroup = ({ id }) => async (dispatch, getState, { apiClient }) => {
    dispatch(getGroupActions.start());
    try {
        const result = await apiClient.getGroup({ id });
        return dispatch(getGroupActions.success(result));
    } catch (error) {
        dispatch(getGroupActions.failure(error.message));
    }
};
export const createGroup = ({ userId, data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(createGroupActions.start());
    try {
        const result = await apiClient.createGroup({ userId, data });
        return dispatch(createGroupActions.success(result));
    } catch (error) {
        dispatch(createGroupActions.failure(error.message));
    }
};
export const updateGroup = ({ id, data }) => async (dispatch, getState, { apiClient }) => {
    dispatch(updateGroupActions.start());
    try {
        const result = await apiClient.updateGroup({ id, data });
        return dispatch(updateGroupActions.success(result));
    } catch (error) {
        dispatch(updateGroupActions.failure(error.message));
    }
};
export const deleteGroup = ({ id }) => async (dispatch, getState, { apiClient }) => {
    dispatch(deleteGroupActions.start());
    try {
        const result = await apiClient.deleteGroup({ id });
        return dispatch(deleteGroupActions.success(result));
    } catch (error) {
        dispatch(deleteGroupActions.failure(error.message));
    }
};

export const reducer = createReducer({
    [getGroupActions.start]: (state, payload) => ({ ...state, loading: true }),
    [getGroupActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [getGroupActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [createGroupActions.start]: (state, payload) => ({ ...state, loading: true }),
    [createGroupActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [createGroupActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [updateGroupActions.start]: (state, payload) => ({ ...state, loading: true }),
    [updateGroupActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [updateGroupActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload }),
    [deleteGroupActions.start]: (state, payload) => ({ ...state, loading: true }),
    [deleteGroupActions.success]: (state, payload) => ({ loading: false }),
    [deleteGroupActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload })
}, {});
