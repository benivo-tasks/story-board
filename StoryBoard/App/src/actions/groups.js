import { createActions } from "../utils/createActions";
import { createReducer } from "redux-act";

const groupsActions = createActions("GET_GROUPS");

export const getGroups = () => async (dispatch, getState, { apiClient }) => {
    dispatch(groupsActions.start());
    try {
        const groups = await apiClient.getGroups();
        return dispatch(groupsActions.success(groups));
    } catch (error) {
        dispatch(groupsActions.failure(error.message));
    }
};

export const reducer = createReducer({
    [groupsActions.start]: (state, payload) => ({ ...state, loading: true }),
    [groupsActions.success]: (state, payload) => ({ data: payload, loading: false }),
    [groupsActions.failure]: (state, payload) => ({ ...state, loading: false, error: payload })
}, {});
