const apiConfig = {
    rootUrl: "http://localhost:63174",
    apiBasePath: "/api",
    resources: {
        users: [
            {
                url: "/users/search",
                method: "GET",
                action: "getUserByName"
            },
            {
                url: "/users",
                method: "GET",
                action: "getUsers"
            },
            {
                url: "/users/:id",
                method: "GET",
                action: "getUser"
            },
            {
                url: "/users",
                method: "POST",
                action: "createUser"
            },
            {
                url: "/users/:id",
                method: "PUT",
                action: "updateUser"
            }
        ],
        stories: [
            {
                url: "/users/:userId/stories",
                method: "GET",
                action: "getStories"
            },
            {
                url: "/stories/:id",
                method: "GET",
                action: "getStory"
            },
            {
                url: "/users/:userId/stories",
                method: "POST",
                action: "createStory"
            },
            {
                url: "/stories/:id",
                method: "PUT",
                action: "updateStory"
            }
        ],
        groups: [
            {
                url: "/groups",
                method: "GET",
                action: "getGroups"
            },
            {
                url: "/groups/:id",
                method: "GET",
                action: "getGroup"
            },
            {
                url: "/users/:userId/groups",
                method: "POST",
                action: "createGroup"
            },
            {
                url: "/groups/:id",
                method: "PUT",
                action: "updateGroup"
            }
        ]
    }
}
export default apiConfig;
