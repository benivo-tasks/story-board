import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import injectTapEventPlugin from 'react-tap-event-plugin';
import storeCreator from "./store";
import ApiClient from "./utils/ApiClient";
import apiConfig from "./apiConfig";
import createApp from "./App";

injectTapEventPlugin();

const apiClient = new ApiClient(apiConfig);
const store = storeCreator({ apiClient });

const root = createApp(store);

ReactDOM.render(root, document.getElementById('root'));
registerServiceWorker();
