import React, { PureComponent } from "react";

const itemListCreator = (Component) => {
    return class ItemList extends PureComponent {
        render() {
            const { items = [] } = this.props;
            return (
                <div>
                    {items.map(item => <Component key={item.id} item={item} />)}
                </div>
            );
        }
    }
};

export default itemListCreator;
