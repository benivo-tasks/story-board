import React, { PureComponent } from "react";
import { Card, CardTitle, CardText } from "material-ui";

class Group extends PureComponent {
    render() {
        const { item: { id, name, description, storyCount = 0, userCount = 0 } } = this.props;
        return (
            <Card key={id}>
                <CardTitle title={name} subtitle={description} />
                <CardText>{`Stories: ${storyCount}`}</CardText>
                <CardText>{`Users: ${userCount}`}</CardText>
            </Card>
        );
    }
}

export default Group;
