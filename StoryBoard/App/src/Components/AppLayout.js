import React, { PureComponent } from "react";
import { Toolbar, ToolbarGroup } from "material-ui";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import StoryContainer from "../Containers/StoryContainer";
import StoryListContainer from "../Containers/StoryListContainer";
import GroupContainer from "../Containers/GroupContainer";
import GroupListContainer from "../Containers/GroupListContainer";
import UserContainer from "../Containers/UserContainer";

class AppLayout extends PureComponent {
    render() {
        const { title = "StoryBoard", user } = this.props;
        return (
            <Router>
                <div>
                    <header>
                        <Toolbar style={{ height: 100 }}>
                            <ToolbarGroup>
                                <Link to="/">
                                    <h1>{title}</h1>
                                </Link>
                            </ToolbarGroup>
                            <ToolbarGroup>
                                <UserContainer />
                            </ToolbarGroup>
                            <ToolbarGroup>
                                {user &&
                                    <nav>
                                        <ul style={{ listStyleType: "none", display: "flex", justifyContent: "space-between", width: 400 }}>
                                            <li><Link to="/groups">Groups</Link></li>
                                            <li><Link to="/stories">Stories</Link></li>
                                            <li>
                                                <Route path="/groups" render={() => <Link to="/groups/create">Create Group</Link>} />
                                                <Route path="/stories" render={() => <Link to="/stories/create">Create Story</Link>} />
                                            </li>
                                        </ul>
                                    </nav>
                                }
                            </ToolbarGroup>
                        </Toolbar>
                    </header>
                    {user &&
                        <main>
                            <Route path="/stories" exact component={StoryListContainer} />
                            <Route path="/stories/create" component={StoryContainer} />
                            <Route path="/stories/:id(\d+)/edit" component={StoryContainer} />
                            <Route path="/groups/create" component={GroupContainer} />
                            <Route path="/groups" exact component={GroupListContainer} />
                        </main>
                    }
                </div>
            </Router>
        );
    }
}

export default AppLayout;