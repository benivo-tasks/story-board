import React, { PureComponent } from "react";
import { TextField, RaisedButton } from "material-ui";

class EditableGroup extends PureComponent {
    state = { id: 0, name: "", description: "" };
    componentDidMount() {
        const { item } = this.props;
        if (item) {
            this.setState(item);
        }
    }
    componentWillReceiveProps(nextProps) {
        const { id } = this.state;
        const { item } = nextProps;
        if (!item) {
            return;
        }
        const { id: nextId } = item;
        if (id !== nextId) {
            this.setState(item);
        }
    }
    handleChange = ({ target: { name } }, value) => {
        this.setState({ [name]: value });
    };

    handleSubmit = () => {
        const { createGroup, isNew, user, onCreated } = this.props;
        if (isNew) {
            createGroup({ userId: user.id, data: this.state });
            onCreated && onCreated();
        }
    };
    render() {
        const { title, description } = this.state;
        return (
            <div>
                <div style={{ display: "flex", flexDirection: "column", width: "80%", margin: "auto" }}>
                    <TextField name="name" floatingLabelText="Name" value={title} onChange={this.handleChange} style={{ width: "50%" }} />
                    <TextField name="description" floatingLabelText="Description" multiLine rowsMax={5} rows={2} value={description} onChange={this.handleChange} style={{ width: "50%" }} />
                    <RaisedButton label="Create" onClick={this.handleSubmit} primary style={{ width: 76 }} />
                </div>
            </div>
        );
    }
}

export default EditableGroup;