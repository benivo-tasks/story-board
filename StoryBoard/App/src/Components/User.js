import React, { PureComponent } from "react";
import { TextField, RaisedButton } from "material-ui";

class User extends PureComponent {
    state = { id: 0, name: "" };
    componentDidMount() {
        const { item } = this.props;
        if (item) {
            this.setState(item);
        }
    } 
    componentWillReceiveProps(nextProps) {
        const { id } = this.state;
        const { item } = nextProps;
        if (!item) {
            return;
        }
        const { id: nextId } = item;
        if (id !== nextId) {
            this.setState(item);
        }
    }
    handleChange = ({ target: { name } }, value) => {
        this.setState({ [name]: value });
    };

    handleSubmit = () => {
        console.log(this.state);
        const { createUser } = this.props;
        createUser({ data: this.state })
    };
    render() {
        const { name } = this.state;
        return (
            <div>
                <div style={{ display: "flex", flexDirection: "column", width: "80%", margin: "auto" }}>
                    <TextField name="name" floatingLabelText="Name" value={title} onChange={this.handleChange} style={{ width: "80%" }} />
                </div>
                <RaisedButton label="Create" onClick={this.handleSubmit} primary />
            </div>
        );
    }
}

export default User;