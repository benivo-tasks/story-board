import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import { Card, CardTitle, CardText, CardActions } from "material-ui";

class Story extends PureComponent {
    render() {
        const { item } = this.props;
        const dateString = new Date(item.postedOn).toLocaleDateString("en-US", { day: "numeric", month: "short", year: "numeric" });
        return (
            <Card key={item.id}>
                <CardTitle title={item.title} subtitle={dateString} />
                <CardText>{item.description}</CardText>
                <CardActions>
                    <Link to={`/stories/${item.id}/edit`}>Edit</Link>
                </CardActions>
            </Card>
        );
    }
}

export default Story;
