import React, { PureComponent } from "react";
import { RaisedButton, TextField } from "material-ui";

class InputWithSubmit extends PureComponent {
    state = { value: "" };
    componentDidMount() {
        const { value } = this.props;
        if (value) {
            this.setState({ value });
        }
    }
    handleChange = (e, value) => {
        this.setState({ value });
    };
    handleSubmit = () => {
        const { onSubmit } = this.props;
        onSubmit(this.state.value);
    };
    render() {
        const { label, errorText } = this.props;
        return (
            <div>
                <TextField floatingLabelText={label} errorText={errorText} onChange={this.handleChange} />
                <RaisedButton primary label="Submit" onClick={this.handleSubmit} />
            </div>
        );
    }
}

export default InputWithSubmit;
