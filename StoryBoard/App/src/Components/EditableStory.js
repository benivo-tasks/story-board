import React, { PureComponent } from "react";
import { TextField, RaisedButton, SelectField, MenuItem } from "material-ui";

class EditableStory extends PureComponent {
    initialState = { id: 0, title: "", description: "", content: "", groupIds: [] };
    state = this.initialState;
    componentDidMount() {
        const { item } = this.props;
        if (item) {
            this.setState(item);
        } else {
            this.setState(this.initialState);
        }
    }
    componentWillReceiveProps(nextProps) {
        const { id } = this.state;
        const { item } = nextProps;
        if (!item) {
            this.setState(this.initialState);
            return;
        }
        const { id: nextId } = item;
        if (id !== nextId) {
            this.setState(item);
        }
    }
    handleChange = ({ target: { name } }, value) => {
        this.setState({ [name]: value });
    };
    handleSelectGroup = (event, index, values) => {
        this.setState({ groupIds: values });
    };

    handleSubmit = () => {
        const { updateStory, createStory, isNew, user } = this.props;
        isNew ? createStory({ userId: user.id, data: this.state }) : updateStory({ id: this.state.id, data: this.state });
    };
    render() {
        const { title, description, content, groupIds } = this.state;
        const { isNew, groups = [] } = this.props;
        return (
            <div>
                <div style={{ display: "flex", flexDirection: "column", width: "80%", margin: "auto" }}>
                    <TextField name="title" floatingLabelText="Title" value={title} onChange={this.handleChange} style={{ width: "50%" }} />
                    <TextField name="description" floatingLabelText="Description" multiLine rowsMax={5} rows={2} value={description} onChange={this.handleChange} style={{ width: "50%" }} />
                    <TextField name="content" floatingLabelText="Content" multiLine rowsMax={50} rows={10} value={content} onChange={this.handleChange} style={{ width: "50%" }} />
                    <SelectField floatingLabelText="Groups" onChange={this.handleSelectGroup} multiple value={groupIds}>
                        {groups.map(g => <MenuItem key={g.id} value={g.id} primaryText={g.name} checked={groupIds.includes(g.id)} />)}
                    </SelectField>
                    <RaisedButton label={isNew ? "Create" : "Update"} onClick={this.handleSubmit} primary style={{ width: 76 }} />
                </div>
            </div>
        );
    }
}

export default EditableStory;