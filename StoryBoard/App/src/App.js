import React from 'react'; import { Provider } from "react-redux";
import AppLayoutContainer from "./Containers/AppLayoutContainer";
import { MuiThemeProvider } from "material-ui/styles";
import "./main.css";

const createApp = (store) => (
  <Provider store={store}>
    <MuiThemeProvider>
      <AppLayoutContainer title="StoryBoard" />
    </MuiThemeProvider>
  </Provider >
);

export default createApp;