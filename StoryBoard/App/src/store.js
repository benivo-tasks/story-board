import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";
import { reducer as user } from "./actions/user";
import { reducer as stories } from "./actions/stories";
import { reducer as story } from "./actions/story";
import { reducer as groups } from "./actions/groups";
import { reducer as group } from "./actions/group";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const storeCreator = (deps, initalState) => createStore(
    combineReducers({ user, stories, story, groups, group }),
    composeEnhancers(applyMiddleware(thunk.withExtraArgument(deps))),
    initalState
);

export default storeCreator;