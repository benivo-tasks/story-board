import { connect } from "react-redux";
import AppLayout from "../Components/AppLayout";

export default connect(
    (state) => ({ user: state.user.data })
)(AppLayout);
