import React, { PureComponent } from "react";
import { connect } from "react-redux";
import InputWithSubmit from "../Components/InputWithSubmit";
import { getUser } from "../actions/user";

class UserContainer extends PureComponent {
    componentDidMount() {
        // const { getUser } = this.props;
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.items !== this.props.items) {
            // const { getGroups } = this.props;
        }
    }
    handleSubmit = (name) => {
        const { getUser } = this.props;
        getUser({ name });
    };
    render() {
        const { user, error } = this.props;
        return (
            user ? <h3>{user.name}</h3>
                :
                <InputWithSubmit label="Enter name" onSubmit={this.handleSubmit} errorText={error} />
        );
    }
}

export default connect(
    (state) => ({
        user: state.user && state.user.data,
        error: state.user.error
    }),
    { getUser }
)(UserContainer);
