import React, { PureComponent } from "react";
import { connect } from "react-redux";
import itemListCreator from "../Components/itemListCreator";
import Story from "../Components/Story";
import { getStories } from "../actions/stories";

const StoryList = itemListCreator(Story);

class StoryListContainer extends PureComponent {
    componentDidMount() {
        const { getStories, userId } = this.props;
        getStories({ userId });
    }
    componentWillReceiveProps(nextProps) {
        const { getStories, userId } = this.props;
        const { userId: nextUserId } = nextProps;
        if (nextUserId !== undefined && nextUserId !== userId) {
            getStories({ userId: nextUserId });
        }
    }
    render() {
        const { items } = this.props;
        return <StoryList items={items} />;
    }
}

export default connect(
    (state) => ({
        items: state.stories.data,
        userId: state.user.data && state.user.data.id
    }),
    { getStories }
)(StoryListContainer);
