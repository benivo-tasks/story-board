import React, { PureComponent } from "react";
import { connect } from "react-redux";
import EditableStory from "../Components/EditableStory";
import { getStory, createStory, updateStory, clearStory } from "../actions/story";
import { getGroups } from "../actions/groups";

class StoryContainer extends PureComponent {
    componentDidMount() {
        const { getStory, getGroups, clearStory, match: { params: { id } }, groups } = this.props;
        if (!groups) {
            getGroups();
        }
        if (id) {
            getStory({ id });
        } else {
            clearStory();
        }
    }
    componentWillReceiveProps(nextProps) {
        const { getStory, getGroups, match: { params: { id } } } = this.props;
        const { match: { params: { id: nextId } }, groups } = nextProps;
        if (id !== nextId) {
            getStory({ id: nextId });
            if (!groups) {
                getGroups();
            }
        }
    }
    render() {
        const { item, createStory, updateStory, groups, user, match: { params: { id } } } = this.props;
        return <EditableStory {...{ item, createStory, updateStory, groups, user }} isNew={!id} />;
    }
}

export default connect(
    (state) => ({
        item: state.story.data,
        groups: state.groups.data,
        user: state.user.data
    }),
    { getStory, createStory, updateStory, getGroups, clearStory }
)(StoryContainer);
