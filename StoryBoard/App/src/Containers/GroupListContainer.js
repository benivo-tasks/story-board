import React, { PureComponent } from "react";
import { connect } from "react-redux";
import itemListCreator from "../Components/itemListCreator";
import Group from "../Components/Group";
import { getGroups } from "../actions/groups";

const GroupList = itemListCreator(Group);

class GroupListContainer extends PureComponent {
    componentDidMount() {
        const { getGroups, userId } = this.props;
        getGroups({ userId });
    }
    componentWillReceiveProps(nextProps) {
        const { getGroups, userId } = this.props;
        const { userId: nextUserId } = this.props;
        if (nextUserId !== undefined && userId !== nextUserId) {
            getGroups({ userId: nextUserId });
        }
    }
    render() {
        const { items } = this.props;
        return <GroupList items={items} />;
    }
}

export default connect(
    (state) => ({
        items: state.groups.data,
        userId: state.user.data && state.user.data.id
    }),
    { getGroups }
)(GroupListContainer);
