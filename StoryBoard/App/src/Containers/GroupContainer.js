import React, { PureComponent } from "react";
import { connect } from "react-redux";
import EditableGroup from "../Components/EditableGroup";
import { getGroup, createGroup } from "../actions/group";

class GroupContainer extends PureComponent {
    componentDidMount() {
        const { getGroup, match: { params: { id } } } = this.props;
        if (id) {
            getGroup({ id });
        }
    }
    componentWillReceiveProps(nextProps) {
        const { getGroup, match: { params: { id } } } = this.props;
        const { match: { params: { id: nextId } } } = nextProps;
        if (id !== nextId) {
            getGroup({ id: nextId });
        }
    }
    handleCreated = () => {
        const { history } = this.props;
        history.push("/groups");
    };
    render() {
        const { item, createGroup, user, match: { params: { id } } } = this.props;
        return <EditableGroup {...{ item, createGroup, user }} isNew={!id} onCreated={this.handleCreated} />;
    }
}

export default connect(
    (state) => ({
        item: state.group.data,
        user: state.user.data
    }),
    { getGroup, createGroup }
)(GroupContainer);
