﻿using Microsoft.EntityFrameworkCore;
using StoryBoard.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoryBoard.Data
{
    public class StoryBoardContext : DbContext
    {
        public StoryBoardContext(DbContextOptions<StoryBoardContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(e => e.Name);

            modelBuilder.Entity<StoryGroup>()
                .HasKey(e => new { e.StoryId, e.GroupId });

            modelBuilder.Entity<StoryGroup>()
                .HasOne(e => e.Story)
                .WithMany(s => s.StoryGroups)
                .HasForeignKey(e => e.StoryId);

            modelBuilder.Entity<StoryGroup>()
                .HasOne(e => e.Group)
                .WithMany(g => g.StoryGroups)
                .HasForeignKey(e => e.GroupId);

            modelBuilder.Entity<UserGroup>()
                .HasKey(e => new { e.UserId, e.GroupId });

            modelBuilder.Entity<UserGroup>()
                .HasOne(e => e.User)
                .WithMany(s => s.UserGroups)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<StoryGroup>()
                .HasOne(e => e.Group)
                .WithMany(g => g.StoryGroups)
                .HasForeignKey(e => e.GroupId);

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<Group> Groups { get; set; }
    }
}

